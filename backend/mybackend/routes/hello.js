const express = require('express')
const router = express.Router()

router.get('/:message', (req, res) => {
    const { params } = req
    res.json({ message: 'Hello Benz.',
                params })
})

module.exports = router