const User = require('../models/User');
const usersController = {
  userList: [
    {
      id: 1,
      name: 'Krerkpon',
      gender: 'M'
    },
    {
      id: 2,
      name: 'Mongkol',
      gender: 'M'
    }
  ],
  lastId: 3,
  async addUser (req, res) {
    const payload = req.body

    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    }catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try{
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch(err) {
      res.status(500).send(err)
    }  
  },
  async deleteUser (req, res) {
    const { id } = req.params
    try{
      const user = await User.deleteOne({_id: id})
      res.json(user)
    }catch (err) {
      res.status(500).send(err)
    }
    
  },
  async getUsers (req, res) {
    res.json(usersController.getUsers());
  // Async Await
  try {
    const users = await User.find({})
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
  },
  async getUser (req, res) {
    const { id } = req.params
    try{
      const user = await User.findById(id)
      res.json(user)
    }catch (err){
      res.status(500).send(err)
    }
  }
}

module.exports = usersController
